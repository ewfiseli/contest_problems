import java.util.Arrays;
import java.util.Scanner;


public class GettingChorded346 {
	static String[] noteMap = {"A", "A#", "B", "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#"};
	static int[] major = {4, 3};
	static int[] minor = {3, 4};
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		while (in.hasNextLine()) {
			String s = in.nextLine();
			if (s.equals(""))
				return;
			int[] notes = getNotes(s);
			Arrays.sort(notes);
			boolean isMajor = false;
			boolean isMinor = false;
			for (int i=0; i < 4; ++i) {
				isMajor = checkMajor(notes);
				isMinor = checkMinor(notes);
				if (isMajor || isMinor)
					break;

				swap(notes);
				isMajor = checkMajor(notes);
				isMinor = checkMinor(notes);
				if (isMajor || isMinor)
					break;
				swap(notes);
				
				rotate(notes);
			}
			if (isMajor)
				System.out.println(s + " is a " + NoteToString(notes[0]) + " Major chord.");
			if (isMinor)
				System.out.println(s + " is a " + NoteToString(notes[0]) + " Minor chord.");
			if (!(isMajor || isMinor))
				System.out.println(s + " is unrecognized.");
		}
	}
	
	private static void swap(int[] notes) {
		int temp = notes[2];
		notes[2] = notes[1];
		notes[1] = temp;
		
	}

	private static boolean checkMinor(int[] notes) {
		int one = notes[0];
		int two = notes[1];
		int three = notes[2];
		if (one > two) 
			two += 12;
		if (two - one != minor[0])
			return false;
		
		two = notes[1];
		
		if (two > three)
			three += 12;
		if (three - two != minor[1])
			return false;
		
		return true;
	}

	private static boolean checkMajor(int[] notes) {
		int one = notes[0];
		int two = notes[1];
		int three = notes[2];
		if (one > two)
			two += 12;
		if (two - one != major[0])
			return false;
		
		two = notes[1];
		
		if (two > three)
			three += 12;
		if (three - two != major[1])
			return false;
		
		return true;
		
	}

	private static void rotate(int[] notes) {
		int one = notes[0];
		int two = notes[1];
		int three = notes[2];
		notes[0] = two;
		notes[1] = three;
		notes[2] = one;
	}

	static int[] getNotes(String s) {
		String[] temp = s.split("\\s+");
		int[] notes = new int[3];
		for (int i=0; i < 3; ++i) {
			String st = temp[i].trim();
			st = st.toUpperCase();
			if (st.length() == 2 && st.charAt(1) == 'B') {
				if (st.charAt(0) == 'A')
					st = "G#";
				else {
					st = ((char) (st.charAt(0) - 1)) + "#";
				}
			}
			notes[i] = StringToNote(st);
		}
			
		return notes;
	}
	
	static int StringToNote(String s) {
		for (int i=0; i < noteMap.length; ++i) {
			if (noteMap[i].equals(s))
				return i;
		}
		System.out.println(s);
		return -1;
	}
	static String NoteToString(int n) {
		return noteMap[n];
	}
}
