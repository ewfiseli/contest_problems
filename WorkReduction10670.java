import java.util.Arrays;
import java.util.Scanner;


public class WorkReduction10670 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int numC = in.nextInt();
		for (int cNum=1; cNum <= numC; ++cNum) {
			int start = in.nextInt();
			int goal = in.nextInt();
			int numP = in.nextInt();
			in.nextLine();
			Prov[] P = new Prov[numP];
			Prov tempP;
			for (int i=0; i < numP; ++i) {
				tempP = new Prov(in.nextLine(), start, goal);
				P[i] = tempP;
			}
			
			Arrays.sort(P);
			System.out.printf("Case %d\n", cNum);
			for (int i=0; i < numP; ++i) 
				System.out.printf("%s %d\n", P[i].name, P[i].cost);
			
		}
	}
	
	static class Prov implements Comparable<Prov> {

		public Prov(String line, int start, int goal) {
			String[] temp = line.split(":");
			name = temp[0];
			temp = temp[1].split(",");
			int cost1 = Integer.parseInt(temp[0]);
			int costHalf = Integer.parseInt(temp[1]);
			
			cost = 0;
			while (start > goal) {
				int startHalf = start/2;
				if (startHalf >= goal && costHalf < cost1*startHalf) {
					start = startHalf;
					cost += costHalf;
				} else {
					start -= 1;
					cost += cost1;
				}
	
			}
			
		}

		@Override
		public int compareTo(Prov p) {
			if (cost - p.cost == 0)
				return name.compareTo(p.name);
			return cost - p.cost;
		}
		
		String name;
		int cost;
		
	}
}
