import java.util.Arrays;
import java.util.Scanner;


public class Bandwidth140 {
	static String ans; 
	static int bandwidth;
	static String[] graph;
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		while (true) {
			String g = in.nextLine();
			g = g.trim();
			if (g.equals("#"))
				break;
			
			bandwidth = 1000;
			ans = "";
			graph = g.split(";");
			String s = g.replaceAll(":", "");
			s = s.replaceAll(";", "");
			String newS = "";
			for (int i=0; i < s.length(); ++i) {
				if (newS.contains(s.substring(i, i+1)) == false)
					newS += s.charAt(i);
			}
			
			char[] sortedS = newS.toCharArray();
			Arrays.sort(sortedS);
			s = new String(sortedS);
			rec("", s);
			String newAns = "";
			for (int i=0; i < ans.length(); ++i)
				newAns += ans.charAt(i) + " ";
			System.out.println(newAns + "-> " + bandwidth);
		}
	}
	
	public static void rec(String s, String left) {
		if (left.length() == 0) {
			generateScore(s);
			return;
		}
		
		for (int i=0; i < left.length(); ++i) {
			rec(s + left.charAt(i), left.replaceFirst(left.substring(i, i+1), ""));
		}
	}
	
	public static void generateScore(String str) {
		int max = 0; 
		for (int i=0; i < graph.length; ++i) {
			String g = graph[i];
			int s_i = str.indexOf(g.charAt(0));
			for (int j=2; j < g.length(); ++j) {
				int dist = Math.abs(s_i - str.indexOf(g.charAt(j)));
				max = Math.max(max, dist);
			}
		}
		
		if (max < bandwidth) {
			ans = str;
			bandwidth = max;
		}
	}
}
