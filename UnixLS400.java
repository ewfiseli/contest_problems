import java.util.Arrays;
import java.util.Scanner;


public class UnixLS400 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		while (in.hasNextLine()) {
			int n = in.nextInt();
			in.nextLine();
			String[] files = new String[n];
			for (int i=0; i < n; ++i)
				files[i] = in.nextLine();
			int max = 0;
			for (int i=0; i < n; ++i)
				max = Math.max(max, files[i].length());
			
			int numCols = 1;
			while (true) {
				int temp =  2*(numCols) + (numCols + 1)*max;
				if (temp <= 60)
					++numCols;
				else
					break;
			}
			Arrays.sort(files);
			printDashes();
			
			int perCol = files.length / numCols;
			if (files.length % numCols != 0)
				++perCol;
			
			for (int i=0; i < perCol; ++i) {
				for (int j=0; j < numCols; ++j) {
					int temp = (j*perCol + i);
					if (temp >= files.length)
						break;
					if (j == numCols - 1) {
						printCol(files[temp], max);
						System.out.print("\n");
					}
					else
						printCol(files[temp], max+2);
				}
				
			}
			
		}
	}
	private static void printDashes() {
		String s = "";
		for (int i=0; i < 60; ++i)
			s += "-";
		System.out.print(s + "\n");
	}
	private static void printCol(String string, int max) {
		while (string.length() < max)
			string += " ";
		System.out.print(string);
	}
	
	
}
