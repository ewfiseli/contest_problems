import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.Scanner;


public class MagicSquarePalindromes11221 {
	public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter out = new BufferedWriter(new OutputStreamWriter(System.out));
		int numCases = Integer.parseInt(in.readLine().trim());
		for (int n=1; n <= numCases; ++n) {
			String s = in.readLine();
			s = s.replaceAll("\\W", "");
			int k = (int) Math.sqrt(s.length());
			boolean isMagic = true;
			if (s.length() % k != 0)
				isMagic = false;
			
			if (isMagic) {
				char[] t1 = s.toCharArray();
				char[] t2 = generateOther(t1, k);
				
				isMagic = (Arrays.equals(t1, t2) && isPalindrome(t1));
			}
			
			out.write("Case #" + n + ":\n");
			if (isMagic)
				out.write(Integer.toString(k) + "\n");
			else
				out.write("No magic :(\n");
				
		}
		out.flush();
	}
	
	static char[] generateOther(char[] s, int k) {
		char[] other = new char[s.length];
		for (int i=0; i < k; ++i) {
			for (int j=0; j < k; ++j) {
				other[k*j + i] += s[i + k*j];
			}
		}
		return other;
		
	}
	
	static boolean isPalindrome(char[] s) {
		int i = 0;
		int j = s.length - 1;
		while (i < j) {
			if (s[i] != s[j])
				return false;
			++i;
			--j;
		}
		return true;
	}
}
