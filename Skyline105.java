import java.util.Scanner;


public class Skyline105 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int[] buildings = new int[10100];
		int min = 0;
		int max = 0;
		while (true) {
			int l = in.nextInt();
			int h = in.nextInt();
			int r = in.nextInt();
			min = Math.min(min, l);
			max = Math.max(max, r);
			for (int i=l; i < r; ++i)
				buildings[i] = Math.max(buildings[i], h);
			in.nextLine();
			if (in.hasNextLine() == false)
				break;
		}
		
		int last = 0;
		String ans = "";
		for (int i=0; i <= max; ++i) {
			if (buildings[i] != last) {
				last = buildings[i];
				ans += i + " " + buildings[i] + " ";
			}
		}
		ans = ans.trim();
		System.out.println(ans);
	}
}
