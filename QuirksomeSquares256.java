import java.util.Scanner;


public class QuirksomeSquares256 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		/*
		for (int n=2; n <= 8; n += 2) {
			int max = 1;
			int half = 1;
			for (int i=0; i < n; ++i) {
				max *= 10;
			}
			for (int i=0; i < n/2; ++i) {
				half *= 10;
			}
			
			System.out.print("{");
			for (int i=0; i < max; ++i) {
				int lower = i % half;
				int upper = i / half;
				int q =(int) Math.pow(upper + lower, 2);
				if(q == i) {
					String s = Integer.toString(i);
					while (s.length() < n)
						s = "0" + s;
					System.out.print("\"" + s + "\"" + ",");
				}
			}
			
			System.out.println("}");
			*/
		
		while (in.hasNextLine()) {
			int x = in.nextInt();
			String[] array = null;
			if (x == 2)
				array = two;
			if (x == 4)
				array = four;
			if (x == 6)
				array = six;
			if (x == 8)
				array = eight;
			
			for (String s : array)
				System.out.println(s);
			
			
		}
	}
	static String[] two = {"00", "01","81"};
	static String[] four = {"0000","0001","2025","3025","9801"};
	static String[] six = {"000000","000001","088209","494209","998001"};
	static String[] eight = {"00000000","00000001","04941729","07441984","24502500","25502500","52881984","60481729","99980001"};
}
