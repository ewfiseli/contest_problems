import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;


public class GenOutput {
	public static void main(String[] args) throws IOException {
		BufferedWriter out = new BufferedWriter(new FileWriter("test.txt"));
		int[] time = {0, 0};
		for (int i=0; i < 60*24; ++i) {
			String hour = Integer.toString(time[0]);
			if (time[0] < 10)
				hour = "0" + hour;
			String minute = Integer.toString(time[1]);
			if (time[1] < 10)
				minute = "0" + minute;
			out.write(hour + ":" + minute);
			
		}
		out.flush();
		out.close();
	}
}

