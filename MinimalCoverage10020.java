import java.util.Scanner;


public class MinimalCoverage10020 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int numC = in.nextInt();
		for (int c=0; c < numC; ++c) {
			int M = in.nextInt();
			int[] array = new int[M];
			int L, R;
			int startL = 0;
			int  startR = 0;
			while (true) {
				L = in.nextInt();
				R = in.nextInt();
				if (L == 0 && R == 0)
					break;
				if (L > 0 && L < M)
					array[L] = Math.max(array[L], R);
				if (L <= 0 && R > startR) {
					startL = L;
					startR = R;
				}		
			}
			
			boolean hasAns = false;
			String ans = "";
			int count = 0;
			if (startR > 0 && startR < M) {
				hasAns = false;
				int lastL, lastR, nextR, nextL;
				nextR = -1;
				nextL =  0;
				lastL = startL;
				lastR = startR;
				ans += startL + " " + startR + "\n";
				++count;
				for (int i=1; i < M; ++i) {
					if (i == lastR + 1) {
						if (nextR == -1) {
							hasAns = false;
							break;
						}
						++count;
						ans += nextL + " " + nextR + "\n";
						lastR = nextR;
						lastL = nextL;
						nextR = -1;
					}
					if (array[i] > lastR && array[i] > nextR) {
						nextR = array[i];
						nextL = i;
					}
					if (nextR >= M) {
						++count;
						ans += nextL + " " + nextR + "\n";
						hasAns = true;
						break;
					}
				}
			}
			if (startR >= M)
				System.out.printf("1\n%d %d\n", startL, startR);
			else if (hasAns == false)
				System.out.printf("0\n");
			else
				System.out.printf("%d\n%s", count, ans);
			
			if (c != numC-1)
				System.out.print("\n");
			
			System.out.flush();
			
		}
	}
}
