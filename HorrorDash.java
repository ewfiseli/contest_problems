import java.util.*;

public class HorrorDash {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int numCases = in.nextInt();
		for (int i=1; i <= numCases; ++i) {
			int max = 0;
			int numRunners = in.nextInt();
			for (int j=0; j < numRunners; ++j) {
				max = Math.max(in.nextInt(), max);
			}
			System.out.println("Case " + i + ": " + max);
		}
	}
}
