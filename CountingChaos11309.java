import java.util.Scanner;


public class CountingChaos11309 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int numCases = in.nextInt();
		in.nextLine();
		for (int c=0; c < numCases; ++c) {
			String t = in.nextLine().trim();
			int[] time = new int[2];
			time[0] = Integer.parseInt(t.substring(0, 2));
			time[1] = Integer.parseInt(t.substring(3));
			addTime(time);
			while (isPalindrome(time) == false) {
				addTime(time);
			}
			String hour = Integer.toString(time[0]);
			if (time[0] < 10)
				hour = "0" + hour;
			String minute = Integer.toString(time[1]);
			if (time[1] < 10)
				minute = "0" + minute;
			
			System.out.println(hour + ":" + minute);
		}
	}

	private static boolean isPalindrome(int[] time) {
		String ans = "";
		int ans_i = time[0]*100;
		ans_i += time[1];
		ans = Integer.toString(ans_i);
		
		int i=0;
		int j=ans.length() - 1;
		while (i < j) {
			if (ans.charAt(i) != ans.charAt(j))
				return false;
			++i;
			--j;
		}
		return true;
	}

	private static void addTime(int[] time) {
		time[1] = (time[1]+1) % 60;
		if (time[1] == 0)
			time[0] = (time[0]+1) % 24;
		
	}
}
