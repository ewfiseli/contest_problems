import java.util.Scanner;


public class LumberjackSequencing11942 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int numCases = in.nextInt();
		System.out.println("Lumberjacks:");
		for (int n=0; n < numCases; ++n) {
			boolean sortedUp = true;
			boolean sortedDown = true;
			int last = in.nextInt();
			int curr;
			for (int i=1; i < 10; ++i) {
				curr = in.nextInt();
				if (last > curr)
					sortedUp = false;
				if (last < curr)
					sortedDown = false;
				last = curr;
			}
			
			if (sortedUp || sortedDown)
				System.out.println("Ordered");
			else
				System.out.println("Unordered");
		}
	}
}
