import java.util.Scanner;


public class SearchingForNessy {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int numCases = in.nextInt();
		for (int i=0; i < numCases; ++i) {
			int num_rows = in.nextInt();
			int num_cols = in.nextInt();
			num_rows -= 2;
			num_cols -= 2;
			int perRow = num_rows/3;
			if (num_rows % 3 != 0)
				++perRow;
			
			int perCol = num_cols/3;
			if (num_cols % 3 != 0)
				++perCol;
			
			System.out.println(perRow * perCol);
		}
	}
}
