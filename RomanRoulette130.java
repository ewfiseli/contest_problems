import java.util.Scanner;


public class RomanRoulette130 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		while (true) {
			int n = in.nextInt();
			int k = in.nextInt();
			if (0 == n && 0 == k)
				return;
			
			int[] array = new int[n];
			
			for (int i=0; i < n; ++i) {
				int numKilled = 0;
				for (int j=0; j < n; ++j)
					array[j] = j;
				
				int index = i;
				int temp;
				boolean safe = true;
				while (numKilled < n-1) {
					index = simulateTurn(array, index, k);
					
					if (array[index] == 0) {
						safe = false;
						break;
					}
				
					
					++numKilled;
					array[index] = -1;
					temp = simulateTurn(array, index, k);

					array[index] = array[temp];
					array[temp] = -1;
					
					if (numKilled == n-1)
						break;
					
					index = (index + 1) % n;

				}
				if (safe) {
					System.out.println(i + 1);
					break;
				}
			}
		}
	}
	
	static int simulateTurn(int[] array, int index, int k) {
		int count = 0;
		while (count < k) {
			if (array[index] != -1)
				++count;
			if (count != k)
				index = (index + 1) % array.length;
		}
		return index;
	}
}
