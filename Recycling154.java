import java.util.Arrays;
import java.util.Scanner;


public class Recycling154 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		while (true) {
			int numCities = 0;
			City[] cities = new City[101];
			while (true) {
				String s = in.nextLine();
				if (s.charAt(0) == 'e')
					break;
				if (s.charAt(0) == '#')
					return;
				
				cities[numCities] = new City(s);
				++numCities;
			}
			
			int min = 1000;
			int city = 0;
			for (int i=0; i < numCities; ++i) {
				int sum =0;
				for (int j=0; j < numCities; ++j) {
					sum += cities[j].numSwaps(cities[i].layout);
				}
				if (sum < min) {
					min = sum;
					city = i+1;
				}
			}
			System.out.println(city);
		}
	}
	
	static class City {
		City(String line) {
			layout = line.split(",");
			Arrays.sort(layout);
		}
		
		int numSwaps(String[] otherLayout) {
			int changes = 0;
			for (int i=0; i < layout.length; ++i) {
				if (layout[i].equals(otherLayout[i]) == false)
					++changes;
			}
			return changes;
		}
		
		public String[] layout;
	}
	
}
