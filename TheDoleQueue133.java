import java.util.Scanner;


public class TheDoleQueue133 {
	static String ans;
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		while (true) {
			int n = in.nextInt();
			int k = in.nextInt();
			int m = in.nextInt();
			if (0 == n && 0 == k && m == 0)
				return;
			
			int pos_l = 0;
			int pos_r = n-1;
			int[] array = new int[n];
			ans = "";
			int left = n;
			while (left > 0) {
				pos_l = simulateLeft(array, pos_l, k);
				pos_r = simulateRight(array, pos_r, m);
				array[pos_l] = 1;
				array[pos_r] = 1;
				if (pos_l == pos_r) {
					left -= 1;
					ans += to3String(pos_l+1) + ',';
				} else {
					left -= 2;
					ans += to3String(pos_l+1) + to3String(pos_r+1) + ',';
				}
			}
			System.out.println(ans.substring(0, ans.length() - 1));
		}
	}
	
	static int simulateRight(int[] array, int i, int m) {
		int count = 0;
		while (count < m) {
			if (array[i] == 0)
				++count;
			if (count != m) {
				--i;
				if (i < 0)
					i = array.length - 1;
			}
		}
		return i;
	}
	
	static int simulateLeft(int[] array, int i, int k) {
		int count = 0;
		while (count < k) {
			if (array[i] == 0)
				++count;
			if (count != k)
				i = (i+1) %array.length;
		}
		return i;
	}
	
	
	static String to3String(int i) {
		String s = Integer.toString(i);
		while (s.length() < 3)
			s = " " + s;
		return s;
	}
}
