import java.util.Scanner;


public class CostCutting11727 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int numCases = in.nextInt();
		for (int i=1; i <= numCases; ++i) {
			int x, y, z, max, min, ans;
			x = in.nextInt();
			y = in.nextInt();
			z = in.nextInt();
			max = Math.max(x, y);
			max = Math.max(max, z);
			min = Math.min(x, y);
			min = Math.min(min, z);
			if (max != x && min != x)
				ans = x;
			else if (max != y && min != y)
				ans = y;
			else
				ans = z;
			System.out.println("Case " + i + ": " + ans);
		}
	}
}
