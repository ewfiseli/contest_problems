import java.util.Scanner;


public class DivisionOfNlogonia {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		while (true) {
			int numQ = in.nextInt();
			if (numQ == 0)
				return;
			int div_x = in.nextInt();
			int div_y = in.nextInt();
			for (int i=0; i < numQ; ++i) {
				int x = in.nextInt();
				int y = in.nextInt();
				String ans;
				if (x == div_x || y == div_y) {
					ans = "divisa";
				} else if(y > div_y) {
					if (x < div_x )
						ans = "NO";
					else
						ans = "NE";
				} else {
					if (x < div_x)
						ans = "SO";
					else 
						ans = "SE";
				}
				
				System.out.println(ans);
			}
		}
		
	}
}
