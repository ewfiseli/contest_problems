import java.util.Arrays;
import java.util.Scanner;


public class WateringGrass10382 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		while (in.hasNextLine()) {
			int n = in.nextInt();
			int len = in.nextInt();
			double width = in.nextDouble();
			width = width / 2;
			Sprinkler[] s = new Sprinkler[n];
			Sprinkler tempS;
			for (int i=0; i < n; ++i) {
				tempS = new Sprinkler(in.nextInt(), in.nextInt(), width);
				s[i] = tempS;
			}
			in.nextLine();
			Arrays.sort(s);
			int count = -1;
			double currR, nextR;
			currR = s[0].right;
			nextR = currR;
			
			if (s[0].left <= 0 && currR >= 0) {
				count = 0;
				for (int i=1; i < n; ++i) {
					if (s[i].left >= currR) {
						if (nextR <= currR) {
							count = -1;
							break;
						}
						++count;
						currR = nextR;
					}
					nextR = Math.max(nextR, s[i].right);
					if (nextR >= len) {
						++count;
						break;
					}
				}
			}
			
			System.out.println(count);
		}
	}
	
	static class Sprinkler implements Comparable<Sprinkler> {
		Sprinkler(int x, int r, double w) {
			left = -1;
			right = -1;
			double temp = r*r - w*w;
			if (temp >= 0) {
				temp = Math.sqrt(temp);
				left = x - temp;
				right = x + temp;
				if (right < left) {
					right = -1;
					left = -1;
				}
			}
			
		}
		
		double left, right;
		@Override
		public int compareTo(Sprinkler o) {
			if (left < o.left)
				return -1;
			if (left > o.left)
				return 1;
			return 0;
		}
		
	}
}
