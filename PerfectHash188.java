import java.util.Arrays;
import java.util.Scanner;


public class PerfectHash188 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		while (in.hasNextLine()) {
			String s = in.nextLine();
			String ts = s.trim();
			String[] temp = ts.split("\\s+");
			
			int[] words = new int[temp.length];
			for (int i=0; i < words.length; ++i) {
				words[i] = getWordScore(temp[i]);
			}
			Arrays.sort(words);
			
			long c = 1;
			long newC;
			while (true) {
				newC = hasConflict(c, words);
				if (newC == -1)
					break;
				
				c = newC;
			}
			System.out.println(s);
			System.out.println(c);
			System.out.println();
			
		}
	}
	
	public static long hasConflict(long c, int[] words) {
		long conf = -1;
		for (int i=0; i  < words.length; ++i) {
			for (int j=i+1; j < words.length; ++j) {
				long x = (long) (Math.floor(c/(double) words[i]) % words.length);
				long y = (long) (Math.floor(c/(double) words[j]) % words.length);
				if (x == y) {
					x = (long) ((Math.floor(c/(double) words[i]) + 1) * words[i] );
					y = (long) ((Math.floor(c/(double) words[j]) + 1) * words[j] );
					x = Math.min(x, y);
					conf = Math.max(conf, x);
				}
			}
		}
			
		return conf;
	}
	
	public static int getWordScore(String s) {
		int score = 0;
		int mul = s.length() - 1;
		
		for (int i=0; i < s.length(); ++i) {
			int ch = s.charAt(i) - 'a' + 1;
			score += ch;
			if (i != s.length() - 1)
				score = score << 5;
			
		}
		return score;
	}
}
