import java.util.Scanner;


public class MaximumSumII10656 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		while (true) {
			int n  = in.nextInt();
			if (n == 0)
				break;
			StringBuffer ans = new StringBuffer(n);
			for (int i=0; i < n; ++i) {
				int x = in.nextInt();
				if (x != 0) {
					ans.append(x);
					ans.append(' ');
				}
			}
			if (ans.length() == 0)
				System.out.println("0");
			else
				System.out.println(ans.toString().trim());
		}
	}
}
