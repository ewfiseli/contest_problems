import java.util.*;

public class ShoemakersProblem10026 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int numC = in.nextInt();
		for (int cc=0; cc < numC; ++cc) {
			int numJ = in.nextInt();
			int[] fine = new int[numJ];
			int[] time = new int[numJ];
			for (int i=0; i < numJ; ++i) {
				time[i] = in.nextInt();
				fine[i] = in.nextInt();
			}
			String ans = "";
			int done = 0;
			double minCost, tempCost;
			int minJob_i;
			while (done < numJ) {
				minJob_i = -1;
				minCost = 0;
				for (int i=0; i < numJ; ++i) {
					if (time[i] != 0) {
						tempCost = time[i] / (double) fine[i];
						if (tempCost < minCost || minJob_i == -1) {
							minJob_i = i;
							minCost = tempCost;
						}
					}
				}
				++done;
				time[minJob_i] = 0;
				ans += (minJob_i + 1) + " ";
			}
			ans = ans.trim();
			System.out.println(ans);
			if (cc != numC-1)
				System.out.println();
			
			
		}
	}
}
