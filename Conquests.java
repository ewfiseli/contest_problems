import java.util.*;


public class Conquests {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int numLines = in.nextInt();
		in.nextLine();
		TreeMap<String, Integer> freq = new TreeMap<String, Integer>();
		for (int i=0; i < numLines; ++i) {
			String s = in.next();
			in.nextLine();
			int x = 1;
			if (freq.containsKey(s)) {
				x += freq.get(s);
			}
			freq.put(s, x);
		}
		
		Iterator<String> iter = freq.navigableKeySet().iterator();
	
		while (iter.hasNext()) {
			String key = iter.next();
			int num = freq.get(key);
			System.out.println(key + " " + num);
		}
	}
}
