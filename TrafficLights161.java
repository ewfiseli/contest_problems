import java.util.Scanner;


public class TrafficLights161 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		Light[] lights = new Light[105];
		int time = 500;

		while (true) {
			time = 500;
			int temp = in.nextInt();
			time = Math.min(temp-5, time);
			int temp2 = in.nextInt();
			time = Math.min(temp2-5, time);
			if (temp == 0 && temp2 == 0)
				return;
			lights[0] = new Light(temp);
			lights[1] = new Light(temp2);
			int l_i = 2;
			while (true) {
				temp = in.nextInt();
				if (temp == 0)
					break;
				lights[l_i++] = new Light(temp);
				time = Math.min(time, temp-5);
			}
			
			int max = 60*60*5;
			boolean found = true;
			for (; time <= max; ++time) {
				found = true;
				for (int i=0; i < l_i; ++i) {
					if (lights[i].isGreen(time) == false) {
						found = false;
						break;
					}	
				}
				if (found)
					break;
			}
			if (found && time <= max) {
				String hours = Integer.toString(time/(60*60));
				if (hours.length() == 1)
					hours = "0" + hours;
				time = time % (60*60);
				
				String minutes = Integer.toString(time/60);
				if (minutes.length() == 1)
					minutes = "0" + minutes;
				time = time % 60;
				
				String seconds = Integer.toString(time);
				if (seconds.length() == 1)
					seconds = "0" + seconds;
				System.out.println(hours + ":" + minutes + ":" + seconds);
			} else {
				System.out.println("Signals fail to synchronise in 5 hours");
			}
				
		}
	}
	
	static class Light {
		public Light(int length) {
			this.length = length;
		}
		
		public boolean isGreen(int time) {
			time = time % (length*2);
			return (time < length - 5);
		}
		
		int length;
	}
	
	
	
	
	
	
	
	
}
