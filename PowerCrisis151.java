import java.util.Scanner;


public class PowerCrisis151 {
	
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		while (true) {
			int n = in.nextInt();
			if (n == 0)
				return;
	
			
			
			int[] array = new int[n];
			long m = 1;
			boolean safe;
			int killed;
			while (true) {
				for (int i=0; i < n; ++i)
					array[i] = 0;
				
				killed = 1;
				array[0] = 1;
				int index = 0;
				safe = true;
				while (killed < n-1) {
					long t = ((m-1) % (n - killed)) + 1;
					index = simulateTurn(array, index, t);
					
					if (index == 12) {
						safe = false;
						break;
					}
					
					++killed;
					array[index] = -1;
				}
				
				if (safe) {
					System.out.println(m);
					break;
				}
				
				++m;
				if (m < 0)
					return;
			}
			
		}
	}
	
	
	static int simulateTurn(int[] array, int index, long k) {
		int count = 0;
		while (count < k) {
			if ((array[index]) == 0)
				++count;
			if (count != k)
				index = (index + 1) % array.length;
		}
		return index;
	}
}
