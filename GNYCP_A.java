import java.util.Scanner;


public class GNYCP_A {
	public static void main(String[] args) {
		long max = 0;
		int[] new_vals = new int[6];
		Scanner in = new Scanner(System.in);
		int numC = in.nextInt();
		for (int i=0; i < numC; ++i) {
			in.nextInt();
			int p_i = 0;
			new_vals[0] = in.nextInt();
			max = new_vals[0];
			while(true) {
				if (new_vals[p_i] == 1)
					break;
				
				int temp = new_vals[p_i];
				if (temp % 2 == 0)
					temp = temp/2;
				else
					temp = 3*temp + 1;
				
				max = Math.max(temp, max);
				p_i = (p_i + 1) % 6;
				new_vals[p_i] = temp;                  
			}
			
			System.out.printf("%d %d\n", i + 1, max);
		}
	}
}
