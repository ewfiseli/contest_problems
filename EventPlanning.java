import java.util.*;

public class EventPlanning {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		while (in.hasNextLine()) {
			int minCost = 0;
			boolean foundAcom = false;
			int numPeople = in.nextInt();
			int budget = in.nextInt();
			int numHotels = in.nextInt();
			int numWeeks = in.nextInt();
			for (int i=0; i < numHotels; ++i) {
				int hotelCost = in.nextInt() * numPeople;
				for (int j=0; j < numWeeks; ++j) {
					int openRooms = in.nextInt();
					if (openRooms >= numPeople && hotelCost <= budget) {
						if (foundAcom == false || (foundAcom == true && hotelCost < minCost)) {
							minCost = hotelCost;
							foundAcom = true;
						}
					}
				}
			}
			
			if (foundAcom)
				System.out.println(minCost);
			else 
				System.out.println("stay home");
			in.nextLine();
		}
	}
}
