import java.util.Scanner;


public class CombinationLock {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		while (true) {
			int start = in.nextInt();
			int c1 = in.nextInt();
			int c2 = in.nextInt();
			int c3 = in.nextInt();
			if (0 == start && 0 == c1 && 0 == c2 && 0 == c3) 
				return;
			
			int sum = 80;
			if (c1 > start) {
				sum += start;
				sum += 40 - c1;
			} else {
				sum += start - c1;
			}
			sum += 40;
			
			if (c2 < c1) {
				sum += 40 - c1;
				sum += c2;
			} else {
				sum += c2 - c1;
			}
			
			if (c3 > c2) {
				sum += c2;
				sum += 40 - c3;
			} else {
				sum += c2 - c3;
			}
			
			double x = 360.0/40.0;
			
			sum *= x;
			System.out.println(sum);
			
			
		}
	}
}
