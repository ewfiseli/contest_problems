import java.util.Scanner;


public class MotherBear10945 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		while (true) {
			String s = in.nextLine();
			s = s.trim();
			if (s.equals("DONE"))
				return;
			s = s.replaceAll("\\W", "");
			s = s.toLowerCase();
			if (isPalindrome(s))
				System.out.println("You won't be eaten!");
			else
				System.out.println("Uh oh..");
		}
	}
	
	static boolean isPalindrome(String s) {
		if (s.length() <= 1)
			return true;
		if (s.charAt(0) != s.charAt(s.length() - 1))
			return false;
		return isPalindrome(s.substring(1, s.length() - 1));
	}
}
