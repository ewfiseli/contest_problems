import java.util.Scanner;


public class GoogleisFeelingLucky12015 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int numCases = in.nextInt();
		in.nextLine();
		for (int n=1; n <= numCases; ++n) {
			String ans = "";
			int max = 0;
			for (int i=0; i < 10; ++i) {
				String url = in.next();
				int rel = in.nextInt();
				in.nextLine();
				if (rel == max)
					ans += "\n" + url;
				if (rel > max) {
					max = rel;
					ans = "\n" + url;
				}
			}
			ans = "Case #" + n + ":" + ans + "\n";
			System.out.print(ans);
		}

	}
}
