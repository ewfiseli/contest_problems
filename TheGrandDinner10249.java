import java.util.Arrays;
import java.util.Scanner;


public class TheGrandDinner10249 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		while (true) {
			int numTeams = in.nextInt();
			int numTables = in.nextInt();
			if (numTeams == 0 && numTables == 0)
				break;
			
			int[] teams = new int[numTeams];
			Table[] tables = new Table[numTables];
			
			for (int i=0; i < numTeams; ++i)
				teams[i] = in.nextInt();
			for (int i=0; i < numTables; ++i) {
				Table temp = new Table();
				temp.size = in.nextInt();
				temp.index = i+1;
				tables[i] = temp;
			}
			Arrays.sort(tables);
			
			String[] teamS = new String[numTeams];
			for (int i=0; i < numTeams; ++i)
				teamS[i] = "";
			
			
			boolean hasSolution = true;
			for (int i=0; i < numTeams; ++i) {
				for (int j=numTables-1; j >= 0; --j) {
					if (teams[i] == 0)
						break;
					if (tables[j].size != 0) {
						--teams[i];
						tables[j].size--;
						teamS[i] += tables[j].index + " ";
					}
				}
				if (teams[i] != 0) {
					hasSolution = false;
					break;
				}
			}
			if (hasSolution) {
				System.out.println("1");
				for (int i=0; i < numTeams; ++i) {
					System.out.println(teamS[i].trim());
				}
			} else {
				System.out.println("0");
			}
			
		}
	}
	
	static class Table implements Comparable<Table> {
		@Override
		public int compareTo(Table t) {
			return (size - t.size);
		}
		int index, size;
	}
}
