import java.util.Scanner;


public class Joseph305 {
	static int[] ans = {2, 7, 5, 30, 169, 441, 1872, 7632, 1740, 93313, 459901, 1358657, 2504881};
	
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		while (true) {
			int k = in.nextInt();
			if (k == 0)
				return;
			//System.out.println(ans[k-1]);
			
			int lastG = k-1;
			int n = 2*k;
			int array = 0;
			long m = 1;
			boolean safe;
			int killed;
			while (true) {
				array = 0;
				killed = 0;
				int index = 0;
				safe = true;
				while (killed < k) {
					long t = ((m-1) % (n - killed)) + 1;
					index = simulateTurn(array, index, t, n);
					
					if (index < k) {
						safe = false;
						break;
					}
					
					++killed;
					array = (array | (1<<index));
				}
				
				if (safe) {
					System.out.print(m + ", ");
					break;
				}
				
				++m;
				if (m < 0)
					return;
			}
			
		}
	}
	
	
	static int simulateTurn(int array, int index, long k, int n) {
		int count = 0;
		while (count < k) {
			if ((array & (1 << index)) == 0)
				++count;
			if (count != k)
				index = (index + 1) % n;
		}
		return index;
	}
}
