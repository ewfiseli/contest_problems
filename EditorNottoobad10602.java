import java.util.ArrayList;
import java.util.Scanner;


public class EditorNottoobad10602 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int numC = in.nextInt();
		for (int cnum = 0; cnum < numC; ++cnum) {
			int numWords = in.nextInt();
			in.nextLine();
			ArrayList<char[]> words = new ArrayList<char[]>(numWords);
			StringBuffer ans = new StringBuffer();
			for (int i=0; i < numWords; ++i)
				words.add(i, in.nextLine().toCharArray());
			
			int count = words.get(0).length;
			int index = 0;
			int nextIndex = -1;
			int maxScore;
			int temp;
			int[] used = new int[numWords];
			used[0] = 1;
			ans.append(words.get(0));
			ans.append('\n');
			for (int n=1; n < numWords; ++n) {
				maxScore = -1;
				for (int i=0; i < numWords; ++i) {
					if (used[i] != 1) {
						temp = getScore(words.get(index), words.get(i));
						if (temp > maxScore) {
							maxScore = temp;
							nextIndex = i;
						}
					}
				}
				
				index = nextIndex;
				count += words.get(index).length - maxScore;
				used[index] = 1;
				ans.append(words.get(index));
				ans.append('\n');
				
			}
			System.out.printf("%d\n%s", count, ans.toString());
		}
	}

	private static int getScore(char[] word, char[] get) {
		int i=0, j=0;
		int max = 0;
		while (i < word.length && j < get.length && word[i] == get[j]) {
			++i;
			++j;
			++max;
		}
		return max;
	}
}
