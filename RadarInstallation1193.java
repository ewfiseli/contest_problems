import java.util.Arrays;
import java.util.Scanner;


public class RadarInstallation1193 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int Cnum = 0;
		while (true) {
			++Cnum;
			int n= in.nextInt();
			int r = in.nextInt();
			if (n == 0 && r == 0)
				break;
			
			Island[] islands = new Island[n];
			Island tempI;
			boolean possible = true;
			for (int i=0; i < n; ++i) {
				tempI = new Island();
				tempI.x = in.nextInt();
				tempI.y = in.nextInt();
				islands[i] = tempI;
				possible = (possible && tempI.y <= r);
			}
			int count = -1;
			if (possible) {
				int i = 0, x, y;
				double left, right, tempL, tempR, temp;
				
				Arrays.sort(islands);
				count = 0;
				while (i < n) {
					x = islands[i].x;
					y = islands[i].y;
					temp = Math.sqrt(r*r - y*y);
					left = x - temp;
					right = x + temp;
					++count;
					++i;
					while (i < n) {
						x = islands[i].x;
						y = islands[i].y;
						temp = Math.sqrt(r*r - y*y);
						tempL = x - temp;
						tempR = x + temp;
						left = Math.max(left, tempL);
						right = Math.min(tempR, right);
						if (left > right)
							break;
						++i;
					}
					
				}
			}
			
			System.out.printf("Case %d: %d\n", Cnum, count);
			
		}
	}
	
	static double maxXDist(int y, double r) {
		return Math.sqrt(r*r - y*y);
	}
	
	static class Island implements Comparable<Island> {
		public int compareTo(Island o) {
			int temp = x - o.x;
			if (temp != 0)
				return temp;
			
			return (o.y - y);
		}
		
		int x, y;
	}
	
}
