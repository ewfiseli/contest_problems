import java.util.Scanner;


public class AllinAll10340 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		while (in.hasNextLine()) {
			String s = in.nextLine().trim();
			String[] temp = s.split("\\s+");
			char[] c = temp[0].toCharArray();
			char[] e = temp[1].toCharArray();
			int c_i = 0;
			int e_i = 0;
			boolean ans = true;
			boolean found = false;
			while (c_i < c.length) {
				found = false;
				while (e_i < e.length) {
					if (c[c_i] == e[e_i]) {
						found = true;
						++e_i;
						break;
					}
					++e_i;
				}
				
				if (!found) {
					ans = false;
					break;
				}
				
				++c_i;
			}
			
			if (ans)
				System.out.println("Yes");
			else
				System.out.println("No");
			
		}
	}
}
