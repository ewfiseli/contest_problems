import java.util.Scanner;


public class MASH402 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int c = 0;
		while (in.hasNextLine()) {
			++c;
			String s = in.nextLine();
			s = s.trim();
			if (s.equals(""))
				return;
			String[] temp = s.split("\\s+");
			
			int n = Integer.parseInt(temp[0]);
			int spots = Integer.parseInt(temp[1]);
			int[] array = new int[n];
			int[] cards = new int[20];
			for (int i=2; i < 22; ++i)
				cards[i-2] = Integer.parseInt(temp[i]);
			int left = n;
			int card_i = 0;
			int i_p = 0;
			int k = cards[0];
			while (left > spots) {
				i_p = simulateTurn(array, i_p, k);
				if (i_p == -1) {
					++card_i;
					if (card_i >= 20)
						break;
					k = cards[card_i];
					i_p = 0;
				} else {
					--left;
					array[i_p] = 1;
				}
			}
			String ans = "";
			for (int i=0; i < n; ++i) {
				if (array[i] == 0)
					ans += (i+1) + " ";
			}
			ans = ans.trim();
			System.out.println("Selection #" + c);
			System.out.println(ans);
			System.out.println();
		}
	}

	private static int simulateTurn(int[] array, int i, int k) {
		int count = 0;
		while (count < k) {
			if (array[i] == 0)
				++count;
			if (count != k)
				++i;
			if (i == array.length)
				return -1;
		}
		return i;
	}
}
