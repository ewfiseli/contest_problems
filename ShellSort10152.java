import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;


public class ShellSort10152 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int numC = in.nextInt();
		for (int c=0; c < numC; ++c) {
			int numT = in.nextInt();
			in.nextLine();
			String[] turtlesString = new String[numT];
			String[] turtleString2 = new String[numT];
			int[] turtles = new int[numT];
			int[] buffer = new int[numT];
			int[] placement = new int[numT];
			HashMap<String, Integer> map = new HashMap<String, Integer>();
			for(int i=0; i < numT; ++i)
				turtlesString[i] = in.nextLine();
			
			for (int i=0; i < numT; ++i) {
				turtleString2[i] = in.nextLine();
				map.put(turtleString2[i], i);
				placement[i] = i;
			}
			for (int i=0; i < numT; ++i) {
				turtles[i] = map.get(turtlesString[i]);
			}
			
			while (Arrays.equals(turtles, placement) == false) {
				int i = findMin(turtles);
				System.out.println(turtleString2[turtles[i]]);
				buffer[0] = turtles[i];
				for (int j=1; j <= i; ++j)
					buffer[j] = turtles[j-1];
				for (int j=i+1; j < numT; ++j)
					buffer[j] = turtles[j];
				
				int[] temp = turtles;
				turtles = buffer;
				buffer = temp;
			}
			
			
			System.out.println();
		}
	}

	private static int findMin(int[] turtles) {
		int max_i = -1;
		int max_v = -1;
		int max_b = turtles[0];
		for (int i=1; i < turtles.length; ++i) {
			if (turtles[i] < max_b && (max_v == -1 || turtles[i] > max_v)) {
				max_v = turtles[i];
				max_i = i;
			}
			max_b = Math.max(max_b, turtles[i]);
		}
		return max_i;
	}

}
