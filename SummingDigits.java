import java.util.Scanner;


public class SummingDigits {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		while (true) {
			String s = in.nextLine();
			s = s.trim();
			if (s.equals("0"))
				return;
			
			int num = Integer.parseInt(s);
			while (true) {
				num = sumDigits(num);
				if (num < 10)
					break;
			}
			System.out.println(num);
		}
	}
	
	static int sumDigits(int num) {
		int sum = 0;
		while (num != 0) {
			sum += num % 10;
			num = num / 10;
		}
		return sum;
	}
}
