import java.util.Scanner;


public class RelationalOperators {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int numCases = in.nextInt();
		for (int i=0; i < numCases; ++i) {
			int num_1 = in.nextInt();
			int num_2 = in.nextInt();
			if (num_1 == num_2) 
				System.out.println("=");
			if (num_1 < num_2)
				System.out.println("<");
			if (num_1 > num_2)
				System.out.println(">");
			
		}
	}
}
