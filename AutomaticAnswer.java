import java.util.Scanner;


public class AutomaticAnswer {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int numCases = in.nextInt();
		for (int i=0; i < numCases; ++i) {
			long num = in.nextInt();
			num *= 567;
			num /= 9;
			num += 7492;
			num *= 235;
			num /= 47;
			num -= 498;
			num /= 10;
			System.out.println(Math.abs(num % 10));
		}
	}
}
