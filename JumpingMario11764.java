import java.util.Scanner;


public class JumpingMario11764 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int numCases = in.nextInt();
		for (int n=1; n <= numCases; ++n) {
			int numWalls = in.nextInt();
			int wall_1, wall_2;
			int sum_high = 0;
			int sum_low = 0;
			wall_1 = in.nextInt();
			for (int i=1; i < numWalls; ++i) {
				wall_2 = in.nextInt();
				if (wall_2 > wall_1)
					++sum_high;
				if (wall_2 < wall_1)
					++sum_low;
				wall_1 = wall_2;
			}
			System.out.println("Case " + n + ": " + sum_high + " " + sum_low);
		}
	}
}
