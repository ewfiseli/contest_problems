import java.util.Scanner;


public class GNYPC_B {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int numC = in.nextInt();
		for (int c=0; c < numC; ++c) {
			in.nextInt();
			int base = in.nextInt();
			char[] num = in.nextLine().trim().toCharArray();
			int mod = 0;
			long base_conv = 1;
			for (int i=num.length-1; i >= 0; --i) {
				mod = (int) ((mod + (num[i]-48)*base_conv) % (base-1));
				base_conv = (base_conv*base) % (base-1);
			}
			System.out.printf("%d %d\n", c+1, mod);
		}
	}
}
