import java.util.Scanner;


public class Safebreaker296 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int numCases = in.nextInt();
		for (int n=0; n < numCases; ++n) {
			int numG = in.nextInt();
			in.nextLine();
			String[] guesses = new String[numG];
			for (int i=0; i < numG; ++i) {
				guesses[i] = in.nextLine();
			}
			boolean found = false;
			boolean found2 = false;
			String ans = "";
			for (int i=0; i < 10000; ++i) {
				String s = createString(i);
				boolean isValid = works(guesses, s);
				if (found && isValid)
					found2 = true;
				if (isValid) {
					ans = s;
					found = true;
				}
			}
			
			if (found == false)
				System.out.println("impossible");
			if (found2)
				System.out.println("indeterminate");
			if (found && !found2)
				System.out.println(ans);
			
		}
	}
	
	static boolean works(String[] g, String s) {
		for (int i=0; i < g.length; ++i) {
			int numRight = Integer.parseInt(g[i].substring(7, 8));
			int numRightDig = Integer.parseInt(g[i].substring(5, 6));
			int[] mask = new int[10];
			int numSharedDig = 0;
			for (int j=0; j < 4; ++j) {
				if (s.charAt(j) == g[i].charAt(j))
					++numSharedDig;
			}
			if (numSharedDig < numRightDig)
				return false;
			
			
		}
		
		return true;
	}
	
	
	static String createString(int i) {
		String s = Integer.toString(i);
		while(s.length() < 4)
			s = "0" + s;
		return s;
	}
}
