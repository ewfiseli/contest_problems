import java.util.Arrays;
import java.util.Scanner;


public class StationBalance410 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int numC = 0;
		while (in.hasNextLine()) {
			++numC;
			int C = in.nextInt();
			int S = in.nextInt();
			int S2 = 2*C;
			int[] array = new int[S2];
			for (int i=S2-S; i < S2; ++i) {
				array[i] = in.nextInt();
			}
			in.nextLine();
			Arrays.sort(array);
			int sum = 0;
			int i=0;
			int j=array.length-1;
			System.out.printf("Set #%d\n", numC);
			int c_i = 0;
			int[] C_array = new int[C];
			while (i < j) {
				sum += array[i] + array[j];
				C_array[c_i] = array[i] + array[j];
				if (array[i] != 0)
					System.out.printf(" %d: %d %d\n", c_i, array[i], array[j]);
				else if (array[j] != 0)
					System.out.printf(" %d: %d\n", c_i, array[j]);
				else 
					System.out.printf(" %d:\n", c_i);
				++i;
				--j;
				++c_i;
			}
			double average = sum / (double) C;
			double imbalance = 0.0;
			for (i=0; i < C; ++i) 
				imbalance += Math.abs(C_array[i] - average);
			System.out.printf("IMBALANCE = %.5f\n", imbalance);
			System.out.println();
		}
	}
}
